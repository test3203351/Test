const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const app = express();

mongoose.connect(
  'mongodb+srv://test:5w1dlkPM7fUg9L5b@cluster0.eefkrs6.mongodb.net/paiteq?retryWrites=true&w=majority',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

app.use(cors());

const userSchema = new mongoose.Schema({
  id: Number,
  name: String,
  age: Number,
});

const User = mongoose.model('User', userSchema);

app.get('/users', async (req, res) => {
  try {
    const { page = 1, limit = 10, sort, filter } = req.query;

    const totalUsers = await User.countDocuments(JSON.parse(filter || '{}'));
    const totalPages = Math.ceil(totalUsers / limit);

    const query = User.find(JSON.parse(filter || '{}'))
      // .sort(JSON.parse(sort || '{}'))
      .limit(Number(limit))
      .skip((Number(page) - 1) * Number(limit));

    const users = await query.exec();

    res.status(200).json({
      totalUsers,
      hasNextPage: Number(page) < totalPages,
      users,
    });
  } catch (error) {
    res
      .status(500)
      .json({ message: 'An error occurred', error: error.toString() });
  }
});

app.listen(process.env.PORT || 6009, () => console.log('Server is running on port 6009'));
